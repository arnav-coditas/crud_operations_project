const popup = document.querySelector(".popupwindow")
const yesDeletebutton = document.querySelector(".yesbutton")
const noDeletebutton = document.querySelector(".nobutton")
const container1 = document.querySelector(".container")

async function Deleteusers() {

    try {
        let response = await fetch(`http://8ddf-103-51-153-190.ngrok.io/getStudents`, {
           
            headers: new Headers({
                
                "ngrok-skip-browser-warning": "1234",
            })
        });
        let userDetails = await response.json();
        userDetails.forEach(e => {
            const deletebtn = document.getElementById(`${e.enrollmentId}`)

                DeleteIt(e.enrollmentId)

       
          

            noDeletebutton.addEventListener("click", () => {
                popup.classList.add("active");
                container1.style.display = ""
            });
        })
    }
    catch {
    }
    
}


async function DeleteIt(enrollmentId)
{
    const deletebtn = document.getElementById(`${enrollmentId}`)
deletebtn.addEventListener("click", () => {
                
    container1.style.display = "none"
    popup.classList.remove("active");
    yesDeletebutton.addEventListener("click", () => {
        fetch(`http://8ddf-103-51-153-190.ngrok.io/deleteStudent/${enrollmentId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            
        })
        popup.classList.remove("active");
        window.location.reload()
      
    });
   

        
        DeleteIt()

    
  
})
}






export default{
    Deleteusers
    

}

