
const addnewformEl = document.querySelector(".addnew-form");
const hidebackgroundEl = document.querySelector(".container");
async function update() {
    try {
        let response = await fetch(`http://8ddf-103-51-153-190.ngrok.io/getStudents`, {
            headers: new Headers({
                "ngrok-skip-browser-warning": "1234",
            })
        });
        let userDetails = await response.json();
        userDetails.forEach(e => {
            const editbtn1 = document.getElementById(`edit${e.enrollmentId}`)
            editbtn1.addEventListener("click", () => {
                addnewformEl.classList.remove("active");
                hidebackgroundEl.classList.add("btnactive");
                let display_users = ""
                let placeholder = document.querySelector("#form-update");
                display_users = `
                <form action="" class="">
                        <label for="Name"></label>
                        <input type="text" name="" id="name" value="${e.studentName}">
                        <label for="age">Age</label>
                        <input type="text" name="" value="${e.age}"id="age">
                        <label for="branch" >Branch</label>
                        <input type="text" name="" id="branch" value="${e.branch}">
                        <label for="percentage">Percentage</label>
                        <input type="number" name="" id="percentage" value="${e.percentage}">
                        <button    type="submit" id="edit" >Submit</button>
                        <button>Close</button>
                </form> 
       `
                placeholder.innerHTML = display_users;
                updatestudentDetails(e.enrollmentId)
            });
        });
    }
    catch {

    }
}




async function updatestudentDetails(id) {
    console.log(id)


    try {
        const submitBtn1 = document.querySelector("#edit");
        submitBtn1.addEventListener("click", () => {
            const Age = document.getElementById("age").value;
            const Branch = document.getElementById("branch").value;
            const Percentage = document.getElementById("percentage").value;
            const Name = document.getElementById("name").value;

            console.log(id, Name, Age, Branch, Percentage)
            
            const response = fetch(`http://8ddf-103-51-153-190.ngrok.io/updateStudent/${id}`, {

                method: 'PUT',
                headers: {
                    "ngrok-skip-browser-warning": "1234",
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    enrollmentId: id,
                    studentName: Name,
                    age: Age,
                    branch: Branch,
                    percentage: Percentage
                })
            }
            )
        })  
    }
    catch {

    }
}


export default {
    update
}
