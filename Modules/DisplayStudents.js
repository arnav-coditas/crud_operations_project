
async function getUsers() {
    try {

        let response = await fetch(`http://8ddf-103-51-153-190.ngrok.io/getStudents`, {
            method: "get",
            headers: new Headers({
                "ngrok-skip-browser-warning": "1234",
            })
        });

        let userDetails = await response.json();

        let placeholder = document.querySelector("#containerTable-body");
        let display_users = "";

        userDetails.forEach(e => {
            // console.log(e)
            display_users += `
                 <tr>
             <td class="tableValues">${e.enrollmentId}</td>
                 <td class="tableValues">${e.studentName}</td>
                 <td class="tableValues">${e.age}</td>
                 <td class="tableValues">${e.branch}</td>
             <td class="tableValues">${e.percentage}</td>
                 <td class="tableValues">
                    <button class="editbutton"id=edit${e.enrollmentId}>Edit</button>
                    <button class="deletebutton" id=${e.enrollmentId}>Delete</button>
                 </td>
             </tr>
        `
            placeholder.innerHTML = display_users;
        });

    }
    catch (err) {

    }
}

export default{
    getUsers,
 
    
}


